from car import Car
from random import random, randint, seed
from state import States

""" nº de unidades de tempo que demora a simulação"""
MAX_TICK = 1000000
MAX_FILAS = 1
MAX_FUEL = 10


def criar_filas(num_filas : int) -> list:
    """ criar estrutura com o nº de filas
    num_filas: nº de filas a criar

    retorna a estrutura com as listas.
    """
    result = []
    for c in range(0, MAX_FILAS):
        result.append([])
    return result


def adicionar_carro_fila(filas:list) -> int:
    """ determinar a qual fila se vai juntar um carro, que sera a fila de menor comprimento

    retorna o valor da fila a juntar-se, ou -1 se não exsitirem filas disponiveis
    """
    result = -1
    f = 999999;
    for c in filas:
        if len(c) < f:
            f = len(c)
            result = ord(c)
    return result


def criar_agentes():
    """ criar o dicionario inicial com as instancias dos agentes
    retorna o dicionário criado, a lista de carros na estrada
    """
    result = {}
    carros_na_estrada = []

    seed()  # reinicializa o gerador de numeros pseudoaleatórios com o relógio de sistema
    for c in range(0, 20):
        result[c] = Car(c, States.onroad, randint(6, 10), randint(2, 6), randint(2, 10))
        carros_na_estrada.append(c)  # todos os carros estão na estrada
    return result, carros_na_estrada


def imprime_estado_agentes(agents: dict, t: int):
    """ imprime o dicionário de agentes
    agents: dicionário de agentes
    t:      instante
    """
    print("\nESTADO DOS AGENTES\nINSTANTE: " + str(t))
    for c in agents:
        print(agents[c])


def executar_simulacao(agents: dict[int, Car], onroad=None, parados: list = None, on_pump: list = None):
    tick = 0
    while tick <= MAX_TICK:
        tick += 1

        """ atualizar os agentes que estão na estrada"""
        for k in agents:
            c = agents[k]
            if c.state == States.onroad:
                c.reduceFuel(1)
                if c.getFuel()==0:
                    c.setState(States.stopped)
                else:
                    if c.on_low_fuel():
                        pass




        """ retirar os agentes das bombas"""
        for fila in on_pump:
            """ se existirem agentes nas bombas, retirar o que está no inicio da fila, atualizando o seu estado"""
            if len(fila) > 0:
                agent = agents[fila.pop(0)]
                agent.setFuel(MAX_FUEL)
                agent.setState(States.onroad)
                """ incrementa o tempo dos restantes carros, e retira os que estão a esperar à demasiado tempo"""
                for c in range(0, len(fila)-1):
                    in_line = agents[c]
                    """ está à espera à demasiado tempo """
                    if in_line.on_stop_wait():
                        in_line.setState(States.onroad)




        if tick % 10 == 0:
            imprime_estado_agentes(agents, tick)


def imprimir_relatório(agents : dict[int, Car]):
    print ("\nrelatório da simulação.\n")


def main():
    """ função principal """
    """ criar a lista de agentes e de carros na estrada"""
    agentes, naestrada = criar_agentes()
    """ criar a lista de carros parados """
    parados = []
    """ criar a lista de carros nas bombas """
    nabomba = criar_filas(MAX_FILAS)

    """ executar a simulacao"""
    executar_simulacao(agents=agentes, onroad=naestrada, parados=parados, on_pump=nabomba)

    imprimir_relatório(agentes)

main()