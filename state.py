from enum import unique, Enum


@unique  # garante valores unicos no enumerador
class States(Enum):
    """ enumerador com os estados possiveis do carro"""
    onroad = 0  # na estrada
    fuelstation = 1  # na bomba
    stopped = 2      # parado sem combustivel
