from state import States

class Car:
    def __init__(self, id, state, fuel, fuel_threshold, waiting_time_threshold):
        """inicializa um agente, neste caso o automóvel"""
        self.id = id
        self.state = state
        self.fuel = fuel                        # combustivel disponivel no carro
        self.fuel_threshold = fuel_threshold    # valor de combustivel a partir do qual o carro retorna à bomba
        self.waiting_time_threshold = waiting_time_threshold  # tempo maximo de espera na bomba
        self.nr_times_bomb = 0
        self.waiting_time = 0

    def __str__(self):
        return str(self.id) + " " + str(self.state) + " " + str(self.fuel) + " " + str(self.fuel_threshold) + " " + \
               str(self.waiting_time_threshold);

    def resetWaitingTime(self):
        """leva para zero o valor de espera"""
        self.waiting_time = 0

    def incrementWaitingTime(self, value):
        """incrementa o valor de espera na fila"""
        self.waiting_time += value

    def getWaitingTime(self):
        """retorna o tempo de espera na fila de combustível"""
        return self.waiting_time

    def getWaitingTimeThreshold(self):
        """retorna o tempo limite de espera na fila de combustível"""
        return self.waiting_time_threshold

    def getId(self):
        """retorna o id do automóvel"""
        return self.id

    def getState(self)->States:
        """retorna o estado"""
        return self.state

    def setState(self, state):
        """atribuí um estado ao automóvel"""
        self.state = state

    def reduceFuel(self, value):
        """reduz o nível de combustível por um valor value"""
        self.fuel -= value

    def getFuel(self):
        """retorna o nível de combustível"""
        return self.fuel

    def setFuel(self, fuel):
        """atribui um valor ao nível de combustível"""
        self.fuel = fuel

    def getFuelThreshold(self):
        """retorna o limite mínimo de combustível"""
        return self.fuel_threshod

    def incrementNrTimesBomb(self):
        """incrementa o número de vez que usou o posto de combustível"""
        self.nr_times_bomb += 1

    def on_low_fuel(self):
        """ determina se um carro tem pouco combustivel"""
        return self.state == States.onroad and self.fuel_threshold >= self.fuel

    def on_stop_wait(self):
        """ determina se um carro já esperou tempo demais """
        return self.state == States.fuelstation and self.waiting_time > self.waiting_time_threshold
